/**
 * main.go
 *
 * Copyright (C) 2019 Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/akamensky/argparse"
)

const (
	waybackMachineBackupURL = "https://web.archive.org/save/"
	waybackMachineStatusURL = "https://archive.org/wayback/available?url="
)

func main() {
	// Create new parser object.
	parser := argparse.NewParser("wayback-machine-archiver",
		"A program to backup of web pages and get its status on Archive.org")

	// Add top level commands `save`.
	backupCmd := parser.NewCommand("backup", "Will backup the given URL")

	// Add top level command `status`.
	statusCmd := parser.NewCommand("status", "Will get the given URL status")

	/**
	 * `parser.List` allows to collect multiple values into the slice of
	 * strings by repeating same flag multiple times.
	 *
	 * Returned data type: `*[]string`
	 */
	linkList := parser.List("u", "url",
		&argparse.Options{Help: "the URL of the web page", Required: true})

	// Parse command line arguments and in case of any error print error and
	// help information.
	err := parser.Parse(os.Args)

	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}

	if backupCmd.Happened() { // Check if `backup` command was given.
		fmt.Println("Started web page backup process...")
		fmt.Println(map_(*linkList, formatBackupURL))
	} else if statusCmd.Happened() { // Check if `status` command was given.
		fmt.Println("Started web page check status process...")
		fmt.Println(map_(*linkList, formatStatusURL))
	} else {
		// In fact we will never hit this one
		// because commands and sub-commands are considered as required.
		err := fmt.Errorf("bad arguments, please check usage")
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}
}

/**
 * `map_` function for Arrays and Slices (similar to Ruby’s `map` method).
 *
 * This function produces a new array of values by mapping each value in list
 * through a transformation function. The transformation function is passed as
 * an arguments into the `map_`.
 */
func map_(list []string, f func(string) string) []string {
	result := make([]string, len(list))

	for i, item := range list {
		result[i] = f(item)
	}

	return result
}

// Given a URL, constructs an Archive URL to submit the archive request.
func formatBackupURL(url string) string {
	requestURL := strings.Join([]string{waybackMachineBackupURL, url}, "")

	return requestURL
}

// Given a URL, constructs an Archive URL to submit the archive request.
func formatStatusURL(url string) string {
	requestURL := strings.Join([]string{waybackMachineStatusURL, url}, "")

	return requestURL
}
